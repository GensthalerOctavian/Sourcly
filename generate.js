$(document).ready(function() {
	$.ajax({
		url: "output.json",
		dataType: "text",
		success: function(data) {
			var input = $.parseJSON(data);
			var entries = input.length;

			var keywords = [ 'Samsung', 'Galaxy', 'Note', 'S-Pen' ];

			// sort this motherfucker based on date.
			input.sort(function(a,b) {
				var firstDate = parseInt(a.date.substr(5, 7));
				var secondDate = parseInt( b.date.substr(5, 7));

				if(firstDate > secondDate)
					return -1;
				if(secondDate > firstDate)
					return 1;
			});
			
			// Go through each element
			for(i = 0; i < entries; i++) {
				var name = input[i].name;
				var title = input[i].title;
				var date = input[i].date;
				var link = input[i].link;
				var passCheck = false;

				for(j = 0; j < keywords.length; j++) 
					if(title.indexOf(keywords[j]) > -1) 
						passCheck = true;
						

				if(passCheck)
					$('#Content .entries-table').append(
						"<tr>" 
						+ "<td data-th='Date'>" + date.substr(5, 20) + "</td>" 
						+ "<td data-th='Source'>" + name + "</td>"
						+ "<td data-th='Title'>" + title + "</td>"
						+ "<td width='100px' data-th='Link'>" + "<a href='" + link + "' target='_blank'>link</a></td>"
						+ "</tr>");
			}
		}
	});
});
